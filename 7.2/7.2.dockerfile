FROM php:7.2

LABEL maintainer="Alvarium.io <alerts@alvarium.io>"

# Install required packages
RUN apt-get update && apt-get install -y \
        unzip \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libcurl4-openssl-dev \
        libpng-dev \
        zlib1g-dev \
        libicu-dev \
        git \
        g++ \
        libsqlite3-dev \
        libbz2-dev && \
    docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ && \
    docker-php-ext-install -j$(nproc) bcmath gd iconv mysqli pdo pdo_mysql pdo_sqlite bz2 zip fileinfo curl mbstring intl sockets && \
    apt-get purge -y \
      libfreetype6-dev \
      libjpeg62-turbo-dev \
      libcurl4-openssl-dev \
      libpng-dev \
      zlib1g-dev \
      libicu-dev \
      libsqlite3-dev \
      libbz2-dev \
    && \
    apt-get autoremove && \
    apt-get clean && \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
